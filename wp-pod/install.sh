#!/bin/bash

#create volumes hostPath
kubectl create -f local-volumes.yaml
#list volumes
kubectl get pv
#create secret for db mysql
kubectl create secret generic mysql-pass --from-literal=password=YOUR_PASSWORD
#list secrets
kubectl get secrets
#create mysql-pod
kubectl create -f wp-mysql.yaml
#list pods
kubectl get pods
#create wp space - pod
kubectl create -f wp.yaml
#show services for wp
kubectl get services wordpress
#get port for external access with minikube
minikube service wordpress --url