Objetives:
Create a PersistentVolume
Create a Secret
Deploy MySQL
Deploy WordPress
Clean up

Info:
MySQL and Wordpress each use a PersistentVolume to store data.
in our case we use: hostPath
Warning: Only use hostPath for developing and testing. With hostPath, 
your data lives on the node the Pod is scheduled onto and does not move between nodes. 
If a Pod dies and gets scheduled to another node in the cluster, the data is lost.