# README #

kubernetes app-db labs

Create an app-db.yaml file and exec:

kubectl create -f app-db.yaml
deployment "app-db" created

create a mongodb Service for the backend, create an services/db-svc.yaml file
and exec:

$ kubectl create -f services/db-svc.yaml
service "mongodb" created